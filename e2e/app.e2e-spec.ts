import { EbjurPage } from './app.po';

describe('ebjur App', function() {
  let page: EbjurPage;

  beforeEach(() => {
    page = new EbjurPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
