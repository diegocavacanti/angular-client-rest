import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/*components*/ 
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { NotFoundComponent } from './shared/not-found.component';


/** services */ 
import { AuthService } from './auth/auth.service';
import { ClienteService } from './clientes/cliente.service';



import { AuthGuard } from './auth/auth.guard';
import { ClienteComponent } from './clientes/cliente-list/cliente.component';
import { ClienteFormComponent } from './clientes/cliente-form/cliente-form.component';
import { ClienteDetailComponent } from './clientes/cliente-detail/cliente-detail.component';
import { PessoaComponent } from './pessoa/pessoa.component';





const APP_ROUTES: Routes = [

  { path: 'clientes', component: ClienteComponent } ,
  { path: 'clientes/new', component: ClienteFormComponent },
  { path: 'clientes/:id', component: ClienteDetailComponent },
  { path: 'clientes/:id/edit', component: ClienteFormComponent },
  { path: 'pessoa', component: PessoaComponent },
  { path: '', pathMatch: 'full', component: HomeComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: 'not-found' },



];
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    ClienteComponent,
    NotFoundComponent,
    ClienteFormComponent,
    ClienteDetailComponent,
    PessoaComponent
],

 imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    ReactiveFormsModule,
    routing
  ],
  
  providers: [
    AuthService, 
    AuthGuard,
    ClienteService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
