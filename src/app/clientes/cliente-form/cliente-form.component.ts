import { ClienteService } from './../cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from './../cliente';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {

  cliente: Cliente = new Cliente();
  title : string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private clienteService: ClienteService

  ) { }

  ngOnInit() {

   let id : any = this.route.params.subscribe(params => {
      let id :any = params['id'];

      this.title = id ? 'Edit User' : 'New User';

      if (!id)        return;

      this.clienteService.getCliente(id)
        .subscribe(
          data => this.cliente = data,
          response => {
            if (response.status == 404) {
              this.router.navigate(['NotFound']);
            }
          });
    });
  }

}