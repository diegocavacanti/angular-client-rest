import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../cliente.service';
import { Cliente } from '../cliente';



@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {


  private clientes: Cliente[] = [];

  constructor(
    private clienteService : ClienteService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.clienteService.getClientes().subscribe(data => this.clientes = data);
  }


 excluir(id: number) {
   if ( confirm('Deseja Excluir cliente '+id) ){

      this.clienteService.deleteCliente(id)
        .subscribe(null,
          err => {
            alert("Não foi possível excluir registro.");
        
          });
   }
	}
}