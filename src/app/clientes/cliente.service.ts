import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class ClienteService {

    private URL: string = 'http://localhost:8080/clientess';

    constructor(private http: Http) { }



    getClientes() {


        return this.http.get(this.URL)
            .map(res => res.json());

    }

    getCliente(id) {
        return this.http.get(this.getClienteUrl(id))
            .map(res => res.json());
    }

    addCliente(cliente) {
        return this.http.post(this.URL, JSON.stringify(cliente))
            .map(res => res.json());
    }

    updateCliente(cliente) {
        return this.http.put(this.getClienteUrl(cliente.id), JSON.stringify(cliente))
            .map(res => res.json());
    }

    deleteCliente(id) {
        return this.http.delete(this.getClienteUrl(id))
            .map(res => res.json());
    }

    private getClienteUrl(id) {
        return this.URL + "/" + id;
    }

}