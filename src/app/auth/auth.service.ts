import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';


@Injectable()
export class AuthService {

  private URL: string = 'http://localhost:8080/rest-server/rest/auth';

  private authenticated = false;

  constructor(private http: Http) { }


  headers() {
    let headersParams = { 'Content-Type': 'application/json' };
    if (localStorage['token']) {
      headersParams['Authorization'] = localStorage['token'];
    }
    let headers = new Headers(headersParams);
    let options = new RequestOptions({ headers: headers });
    return options;
  }

  login(usuario: string, senha: string) {

     return this.http.post(this.URL, JSON.stringify({ "username": usuario, "password": senha }))
      .map( res => res.json() ).catch(this.processarErros);

      
      
  }

  logout() {
    delete localStorage['token'];
  }

  logado() {
    return localStorage['token'];
  }


  extrairDados(response: Response) {
    let data = response.json();
    return data || {};
  }

  processarErros(erro: any) {
    return Observable.throw('Erro ao acessar servidor remoto.');
  }

}
