import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: '<h3>Página não Encontrada!</h3>'
})
export class NotFoundComponent {}
