import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../auth/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  private usuario: string;
  private senha: string;
  private msgErro: string;

  //injecao do serviço
  constructor(
    private router: Router,
    private authService: AuthService) { }



  logar() {
    this.authService.login(this.usuario, this.senha)
      .subscribe(
        token => {
          localStorage['token'] = token;
          console.info(token);
          this.router.navigate(['/home']);
        },
        error => this.msgErro = error
      );
  }



  processarLogin(token: string) {
    localStorage['token'] = token;
    console.info(token);
    this.router.navigate(['/home']);
  }

}
